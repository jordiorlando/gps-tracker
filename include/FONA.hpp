#ifndef FONA_HPP
#define FONA_HPP

#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_FONA.h>

#define FONA_SERIAL Serial1
#define FONA_RST 2
#define FONA_PS  3
#define FONA_KEY 4

bool setupFONA(void);
void queryFONA(void);
void printMenu(void);
void flushSerial(void);
char readBlocking(void);
uint16_t readNumber(void);
uint8_t readLine(char *buff, uint8_t maxbuff, uint16_t timeout = 0);

#endif // FONA_HPP
