#ifndef MMA8451_HPP
#define MMA8451_HPP

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_MMA8451.h>

bool setupMMA8451(void);
void readMMA8451(void);

#endif // MMA8451_HPP
