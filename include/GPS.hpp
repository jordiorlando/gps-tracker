#ifndef GPS_HPP
#define GPS_HPP

#include <Arduino.h>
#include <Adafruit_GPS.h>

#define GPS_SERIAL Serial2

bool setupGPS(void);
void readGPS(void);

#endif // GPS_HPP
