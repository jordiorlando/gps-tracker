#include "FONA.hpp"
#include "GPS.hpp"
#include "MMA8451.hpp"

int main(void) {
  // USB Serial for debugging
  while (!Serial);
  Serial.begin(115200);

  setupFONA();
  setupGPS();
  setupMMA8451();

  while (true) {}

  return 0;
}
