/**************************************************************************/
/*!
    @file     Adafruit_MMA8451.h
    @author   K. Townsend (Adafruit Industries)
    @license  BSD (see license.txt)

    This is an example for the Adafruit MMA8451 Accel breakout board
    ----> https://www.adafruit.com/products/2019

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    @section  HISTORY

    v1.0  - First release
*/
/**************************************************************************/

#include "MMA8451.hpp"

Adafruit_MMA8451 mma = Adafruit_MMA8451();

bool setupMMA8451(void) {
  if (!mma.begin()) {
    Serial.println(F("Couldn't find MMA8451"));
    return false;
  }
  Serial.println(F("Found MMA8451"));

  mma.setRange(MMA8451_RANGE_2_G);
  Serial.print(F("Range = "));
  Serial.print(2 << mma.getRange());
  Serial.println(F("G"));

  return true;
}

void readMMA8451(void) {
  // Read sensor data
  mma.read();

  // Display the raw results (in 14-bit counts)
  Serial.print("X:\t"); Serial.print(mma.x); Serial.print("\t");
  Serial.print("Y:\t"); Serial.print(mma.y); Serial.print("\t");
  Serial.print("Z:\t"); Serial.print(mma.z); Serial.print("\t");
  Serial.println();

  // Get a new sensor event
  sensors_event_t event;
  mma.getEvent(&event);

  // Display the results (acceleration is measured in m/s^2)
  Serial.print("X:\t"); Serial.print(event.acceleration.x); Serial.print("\t");
  Serial.print("Y:\t"); Serial.print(event.acceleration.y); Serial.print("\t");
  Serial.print("Z:\t"); Serial.print(event.acceleration.z); Serial.print("\t");
  Serial.println(F("m/s^2 "));
}
