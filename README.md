# GPS Tracker

A mashup of Adafruit boards ([FONA](https://www.adafruit.com/product/1963), [Ultimate GPS](https://www.adafruit.com/product/746), [Bluefruit LE](https://www.adafruit.com/product/1697), and [MMA8451](https://www.adafruit.com/product/2019)) and a [Teensy 3.1](https://store.oshpark.com/products/teensy-3-1) to drive it all. Motorcycle thieves, beware.
